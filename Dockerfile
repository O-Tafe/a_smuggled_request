FROM ubuntu:20.04

VOLUME ["/dev/log"]

RUN export DEBIAN_FRONTEND=noninteractive && apt update && apt -y upgrade && apt install -y haproxy python3 python3-pip memcached netcat

RUN pip3 install flask gunicorn pymemcache
RUN pip3 install gunicorn[gevent]

COPY haproxy.cfg /etc/haproxy/
COPY haproxy /usr/sbin/



COPY backend.py entrypoint.sh automated.sh /opt/
COPY ./templates/profile.html /opt/templates/
COPY ./static/main.css /opt/static/

WORKDIR /opt/

EXPOSE 80

CMD ["/opt/entrypoint.sh"]
