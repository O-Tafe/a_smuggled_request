# Challenge: A Smuggled Request
A suspect we are tracking has a high volume of traffic hitting one specific web server.
We believe that they are using this machine as a jumpbox.

We are waiting on receiving the logs from the vendor, but we need to solve this now.

Our suspect is extremely cautious, we cannot guarantee that javascript will be enabled in their browser.

Find the method used to backdoor the system to get the flag.


## Challenge Source
To run the challenge:
```bash
docker run --rm otafe/a_smuggled_request
```
or build the docker image from the git repository
```
git clone https://gitlab.com/O-Tafe/a_smuggled_request.git
cd a_smuggled_request
docker build -t a_smuggled_request .
docker run --rm -t a_smuggled_request
```


## Resources
[https://tools.ietf.org/html/rfc7230#page-32](A Relevent RFC)

[https://portswigger.net/web-security/request-smuggling](A portswigger article on the vulnerability)

[https://www.youtube.com/watch?v=w-eJM2Pc0KI](A youtube video on the vulnerability)
