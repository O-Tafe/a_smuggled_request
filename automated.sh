#!/bin/bash

while true
do
	echo -e "GET / HTTP/1.1\r\nHost: challenges.afnom.net\r\nFlag: CyberDiscovery{1N53CUr3_8Y_r3V3r53_Pr0XY}\r\n\r\n" | nc 127.0.0.1 80 1>/dev/null
	sleep 0.01
done
