from flask import Flask, request, jsonify, make_response, render_template, redirect, url_for
from os import urandom
from random import choice
from string import ascii_letters, digits

from pymemcache.client import base

app = Flask(__name__)
app.secret_key = urandom(64)

global client
client = base.Client(('localhost', 11211))

@app.route('/', methods=['GET', 'POST'])
def main():
    global client
    # the next line is required for Transfer-Encoding support in the request
    request.environ['wsgi.input_terminated'] = True
    headers = {}
    bio = ''
    if request.cookies.get('sess_id'):
        bio = client.get(request.cookies.get('sess_id'))
        if bio:
            bio = bio.decode()
    if not bio:
        bio = ''

    for header in request.headers:
        headers[header[0]] = header[1]
    resp = make_response(render_template('profile.html', bio=bio))
    if request.environ['HTTP_HOST'] != '127.0.0.1':
        if not request.cookies.get('sess_id'):
            sess_id = ''.join(choice(ascii_letters+digits) for _ in range(10))
            resp.set_cookie("sess_id", value=sess_id)
            client.set(sess_id, '')

    return resp

@app.route('/update', methods=['POST'])
def update_bio():
    global client
    sess_id = request.cookies.get('sess_id')
    if not sess_id or (not client.get(sess_id) and client.get(sess_id) != b''):
        return "Please start a session"
    bio = request.form.get('bio', '')
    client.set(sess_id, bio)
    if request.environ['HTTP_HOST'] != '127.0.0.1':
        return redirect(url_for('main'))
    return "Bio updated"
