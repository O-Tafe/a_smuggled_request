#!/bin/bash
service haproxy start
service memcached start
bash automated.sh &
gunicorn --keep-alive 10 -k gevent --bind 0.0.0.0:8080 -w 4 backend:app